# Sum
#
# Write an Array method, `sum`, that returns the sum of the elements in the
# array. You may assume that all of the elements are integers.

class Array

  def sum
    return 0 if empty?
    reduce(:+)
  end

end

# Square
#
# Write an array method, `square`, that returns a new array containing the
# squares of each element. You should also implement a "bang!" version of this
# method, which mutates the original array.

class Array

  def square!
    each_index { |i| self[i] *= self[i] }
  end

  def square
    map { |int| int**2 }
  end
end

# Finding Uniques
#
# Monkey-patch the Array class with your own `uniq` method, called
# `my_uniq`. The method should return the unique elements, in the order
# they first appeared:
#
# ```ruby
# [1, 2, 1, 3, 3].my_uniq # => [1, 2, 3]
# ```
#
# Do not use the built-in `uniq` method!

class Array

  def my_uniq
    return_arr = Array.new
    each do |el|
      return_arr << el if return_arr.count(el) < 1
    end
    return_arr
  end



end

# Two Sum
#
# Write a new `Array#two_sum` method that finds all pairs of positions
# where the elements at those positions sum to zero.
#
# NB: ordering matters. I want each of the pairs to be sorted smaller
# index before bigger index. I want the array of pairs to be sorted
# "dictionary-wise":
#
# ```ruby
# [-1, 0, 2, -2, 1].two_sum # => [[0, 4], [2, 3]]
# ```
#
# * `[0, 2]` before `[1, 2]` (smaller first elements come first)
# * `[0, 1]` before `[0, 2]` (then smaller second elements come first)


class Array

  def two_sum
    index_arr = Array.new
    each_with_index do |el1, indx1|
      each_with_index do |el2, indx2|
        index_arr << [indx1, indx2] if (el1 + el2) == 0
      end
    end
    index_arr = index_arr.filter
  end

  def filter
    filtered_arr = self.clean_duplicates!
    filtered_arr = filtered_arr.clean_same_indx_sums!
    filtered_arr
  end

  def clean_duplicates!
    map(&:sort).uniq
  end

  def clean_same_indx_sums!
    select { |nest| nest if nest[0] != nest[-1] }
  end

end

# Median
#
# Write a method that finds the median of a given array of integers. If
# the array has an odd number of integers, return the middle item from the
# sorted array. If the array has an even number of integers, return the
# average of the middle two items from the sorted array.

class Array

  def median
    #[1,2,3,4],[1..2][4], [length/2-1,length/2]
    #[1,2,4,5,6],[length/2]
    sorted_arr = self.sort
    return nil if empty?
    if length.even?
      [sorted_arr[length / 2 - 1], sorted_arr[length / 2]].avg
    elsif length.odd?
      sorted_arr[length / 2]
    end
  end

  def avg
    reduce(:+) / length.to_f
  end

end

# My Transpose
#
# To represent a *matrix*, or two-dimensional grid of numbers, we can
# write an array containing arrays which represent rows:
#
# ```ruby
# rows = [
#     [0, 1, 2],
#     [3, 4, 5],
#     [6, 7, 8]
#   ]
#
# row1 = rows[0]
# row2 = rows[1]
# row3 = rows[2]
# ```
#
# We could equivalently have stored the matrix as an array of
# columns:
#
# ```ruby
# cols = [
#     [0, 3, 6],
#     [1, 4, 7],
#     [2, 5, 8]
#   ]
# ```
#
# Write a method, `my_transpose`, which will convert between the
# row-oriented and column-oriented representations. You may assume square
# matrices for simplicity's sake. Usage will look like the following:
#
# ```ruby
# matrix = [
#   [0, 1, 2],
#   [3, 4, 5],
#   [6, 7, 8]
# ]
#
# matrix.my_transpose
#  # => [[0, 3, 6],
#  #    [1, 4, 7],
#  #    [2, 5, 8]]
# ```
#
# Don't use the built-in `transpose` method!

class Array

  def my_transpose
    transp_matrix = Array.new
    row_indx = 0
    row_size = self[0].count
    row_size.times do
      new_row = Array.new
      each do |row|
        new_row << row[row_indx]
      end
      transp_matrix << new_row
      row_indx += 1
    end
    transp_matrix
  end

end

# Bonus: Refactor your `Array#my_transpose` method to work with any rectangular
# matrix (not necessarily a square one).
